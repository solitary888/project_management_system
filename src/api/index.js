import axios from "axios"

//添加一个全局的拦截器

axios.interceptors.request.use((config) => {
    //如果是登入请求就不需要拦截和携带token了
    if (config.url !== "/api/users/login") {
        config.headers['authorization'] = localStorage.getItem("token")
    }
    return config
}, error => {
    console.log(error)
})

export let login = (data) => {
    return axios({
        url: "/api/users/login",
        method: 'post',
        data: data,
        headers: {
            "Content-Type": "application/json"
        },
        timeout: 1000 * 20
    })
}
//获取班级列表
export const getClasses = () => {
    return axios.get("/api/students/getclasses")
}
//获取学员列表

export const getStudents = (query = {}) => {
    return axios.get("/api/students/getstulist", {
        params: query
    })
}

// 添加学员信息

export const addStu = (data) => {
    return axios({
        url: "/api/students/addstu",
        method: "post",
        data: data
    })
}

//删除学员信息

export const delStu = (sId) => {
    return axios.get(`/api/students/delstu?sId=${sId}`)
}

//更新学员信息
export const editStuInfo = (updated) => axios({
    url: "/api/students/updatestu",
    data: updated,
    method: 'post'
})

//搜索学员信息
export const searchStu = (data) => {
    console.log(data)
   return axios.get(`/api/students/searchstu`, {
        params: data
    })
}

//验证登入状态的接口
export let verify = () => axios.get("/api/verify")