import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

//引入全局样式
import "./styles/index.css"
//引入覆盖element-ui的样式
import "./styles/element-reset.css"
//引入去除元素默认样式
import "./styles/reset.css"
// 粒子特效
import VueParticles from 'vue-particles'
Vue.use(VueParticles)

Vue.config.productionTip = false
Vue.use(ElementUI);

//全局守卫
router.beforeEach((to, from, next) => {
  //1.在用户未登入的状态下 需要拦截(判断token)
  //2.如果用户访问的不是login页面 通通拦截
  let token = localStorage.getItem("token") || "";

  if (token) {
    next()
  } else {
    if (to.path !== '/login') {
      // 如果用户未登录的状态下访问除了login以外的页面 就要拦截掉 并且让它跳到login页
      next({ path: "/login" })
    } else {
      next()
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
